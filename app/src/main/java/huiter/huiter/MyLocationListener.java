package huiter.huiter;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class MyLocationListener implements LocationListener {

    private MainActivity mActivity;

    public MyLocationListener(MainActivity activity){
        this.mActivity = activity;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("provider : ",location.getProvider());
        Log.d("latitude : ",Double.toString(location.getLatitude()));
        Log.d("longitude : ",location.getProvider());
        this.mActivity.displayLocation(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

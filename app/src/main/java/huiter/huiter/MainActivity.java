package huiter.huiter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = AppCompatActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // onCreate  = initialisation de l'activité, tout ce qui n'est pas temps réel, tout ce qui restera actif jusqu'a que l'application soit détruite
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,getString(R.string.app_debug1));
        List<AudioModel> musicList = getAllAudioFromDevice(this.getApplicationContext());
        if(!musicList.isEmpty()){
            Log.e("good","not empty");
            MediaPlayer mp2 = MediaPlayer.create(this, Uri.parse(musicList.get(0).getaPath()));
            //mp2.start();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        MyLocationListener locationListener = new MyLocationListener(this);

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }catch(Error e) {

        }

        Intent intent = new Intent(this,locationListener);
    }

    public List<AudioModel> getAllAudioFromDevice(final Context context) {

        final List<AudioModel> tempAudioList = new ArrayList<>();

        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST,};
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        Cursor c = this.managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);

        if (c != null) {
            while (c.moveToNext()) {
                Log.d("move","MoveToNext");
                AudioModel audioModel = new AudioModel();
                String path = c.getString(0);
                String album = c.getString(1);
                String artist = c.getString(2);

                String name = path.substring(path.lastIndexOf("/") + 1);

                audioModel.setaName(name);
                audioModel.setaAlbum(album);
                audioModel.setaArtist(artist);
                audioModel.setaPath(path);

                if(artist.equals("Narcos")){
                    Log.e("Name :" + name, " Album :" + album);
                    Log.e("Path :" + path, " Artist :" + artist);

                    tempAudioList.add(audioModel);
                }

            }
            c.close();
        }

        return tempAudioList;
    }

    public void displayLocation(Location location) {
        TextView textViewLongitude = (TextView)findViewById(R.id.textLongitude);
        textViewLongitude.setText(Double.toString(location.getLongitude()));

        TextView textViewLatitude = (TextView)findViewById(R.id.textLatitude);
        textViewLatitude.setText(Double.toString(location.getLatitude()));

        TextView v = (TextView)findViewById(R.id.textProvider);
        //v.setText(Double.toString(location.getProvider()));

    }
}
